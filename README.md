Layout Paragraphs Limit
=======================

This module provides an interface to define which paragraph types are not
allowed on certain layout regions used in layout paragraphs.
It requires Layout Paragraphs 2.x to work.

Installation
------------
Install the Layout Paragraphs Limit module as you would normally install a contributed Drupal module.
Visit [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules) for further information.

Configuration
-------------
1. Go to the Drupal admin interface and navigate to
   `admin/config/content/layout-paragraphs-limit`.
2. Go to the layout region for which you want to disallow certain paragraph types.
3. In the paragraph type list, check the types that should not be allowed for
   the selected layout region.
4. Click on the "Save" button to save your changes.

Note: If you disable a paragraph type for a specific layout region, it will not be
available for selection or moving into that region, but it will still be
available for other regions that allow it.
