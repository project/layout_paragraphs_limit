(function (Drupal, drupalSettings, once) {
  Drupal.behaviors.layoutParagraphsLimitDisallowedTypes = {
    attach: function () {
      once('layout-paragraphs-limit-disallowed-types', 'body').forEach(() => {
        Drupal.registerLpbMoveError((settings, el, target, source, sibling) => {
          if (el.classList.contains('paragraph')) {
            const layout = target.closest('.lpb-layout');
            if (layout) {
              const parentLayout = layout.dataset.layout;
              const region = target.dataset.region;

              if (!drupalSettings.layoutParagraphsLimit.disallowedTypes) {
                return;
              }
              const isNegated = drupalSettings.layoutParagraphsLimit.disallowedTypes[parentLayout][region].negate;
              const disallowedTypes = drupalSettings.layoutParagraphsLimit.disallowedTypes[parentLayout][region].paragraph_types;
              if (disallowedTypes) {
                if (
                  (!isNegated && disallowedTypes.hasOwnProperty(el.dataset.type)) ||
                  (isNegated && !disallowedTypes.hasOwnProperty(el.dataset.type))
                ) {
                  return Drupal.t('This paragraph type is not allowed in this region.');
                }
              }

              // Check total numeric limit.
              const numericLimit = drupalSettings.layoutParagraphsLimit.disallowedTypes[parentLayout][region].numeric_limit;
              if (numericLimit) {
                // We want only paragraph components or layout components.
                const descendentComponents = layout.querySelectorAll(`.js-lpb-region[data-region=${region}] .js-lpb-component:not(.js-lpb-region)`);
                let otherDirectChildren = 0;
                descendentComponents.forEach(function (child) {
                  if (
                    // Only count existing direct children.
                    child.parentNode.closest('.lpb-layout').dataset.uuid == layout.dataset.uuid &&
                    // Ignore movement inside the same region.
                    source.dataset.regionUuid != target.dataset.regionUuid
                  ) {
                    otherDirectChildren++;
                  }
                });
                if (otherDirectChildren >= numericLimit) {
                  return Drupal.t('Too many components in this region.');
                }
              }
            }
          }
        });
      });
    }
  };
})(Drupal, drupalSettings, once);
