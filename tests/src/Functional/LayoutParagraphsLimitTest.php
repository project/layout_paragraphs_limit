<?php

namespace Drupal\Tests\layout_paragraphs_limit\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\paragraphs\Entity\ParagraphsType;

/**
 * Tests the Layout Paragraphs Limit configuration UI.
 *
 * @group layout_paragraphs_limit
 */
class LayoutParagraphsLimitTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'paragraphs',
    'layout_paragraphs',
    'layout_paragraphs_limit',
    'node',
    'field',
    'field_ui',
    'block',
  ];

  /**
   * Use a simple core theme for the test.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $user = $this->drupalCreateUser([
      'administer site configuration',
      'administer node fields',
      'administer paragraphs types',
    ]);
    $this->drupalLogin($user);

    $paragraphs_type = ParagraphsType::create([
      'id' => 'layout',
      'label' => 'Layout',
    ]);
    $paragraphs_type->save();

    $paragraphs_type = ParagraphsType::create([
      'id' => 'content_pt',
      'label' => 'Content',
    ]);
    $paragraphs_type->save();

    $this->drupalGet('admin/structure/paragraphs_type/layout');
    $this->submitForm([
      'behavior_plugins[layout_paragraphs][enabled]' => TRUE,
      'behavior_plugins[layout_paragraphs][settings][available_layouts][]' => [
        'layout_onecol'
      ],
    ], 'Save');
    $this->assertSession()->pageTextContains('Saved the layout Paragraphs type.');
  }

  /**
   * Tests that the "Layout: One column" layout is listed, but "Layout: Two column" is not.
   */
  public function testLayoutParagraphsLimitPage() {
    $this->drupalGet('/admin/config/content/layout_paragraphs/limit');

    $this->assertSession()->pageTextContains('Layout: One column');

    $this->assertSession()->pageTextNotContains('Layout: Two column');

    $this->assertSession()->pageTextContains('Region: Content');

    $this->assertSession()->elementExists('css', 'input[name="disallowed_types[layout_onecol][content][numeric_limit]"]');

    $this->assertSession()->elementExists('css', 'input[type="radio"][name="disallowed_types[layout_onecol][content][negate]"]');

    $this->assertSession()->elementExists('css', 'input[name="disallowed_types[layout_onecol][content][paragraph_types][content_pt]"]');
  }

}
