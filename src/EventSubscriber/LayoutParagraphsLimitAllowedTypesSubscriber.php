<?php

declare(strict_types = 1);

namespace Drupal\layout_paragraphs_limit\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\layout_paragraphs\Event\LayoutParagraphsAllowedTypesEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Implements LayoutParagraphsLimitAllowedTypesSubscriber class.
 *
 * @package Drupal\layout_paragraphs_limit
 */
class LayoutParagraphsLimitAllowedTypesSubscriber implements EventSubscriberInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * LayoutParagraphsLimitAllowedTypesSubscriber constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      LayoutParagraphsAllowedTypesEvent::EVENT_NAME => 'typeRestrictions',
    ];
  }

  /**
   * Implements typeRestrictions().
   *
   * @param \Drupal\layout_paragraphs\Event\LayoutParagraphsAllowedTypesEvent $event
   *   The event object.
   */
  public function typeRestrictions(LayoutParagraphsAllowedTypesEvent $event) {
    $parent_uuid = $event->getParentUuid();
    $region = $event->getRegion();
    $parent = $event->getLayout()->getComponentByUuid($parent_uuid);
    if (!$parent) {
      return;
    }
    $parent_settings = $parent->getSettings();
    $parent_layout = $parent_settings['layout'];

    // Get the disallowed paragraph types for the current layout.
    $disallowed_types = $this->configFactory->get('layout_paragraphs_limit.settings')->get('disallowed_types');

    if (!empty($disallowed_types[$parent_layout][$region]['paragraph_types'])) {
      if (empty($disallowed_types[$parent_layout][$region]['negate'])) {
        // If negate is empty/false, we exclude the checked types from the list.
        $allowed_types = array_diff_key($event->getTypes(), array_filter($disallowed_types[$parent_layout][$region]['paragraph_types']));
      }
      else {
        // If negate is true, we include only the checked types from the list.
        $allowed_types = array_intersect_key($event->getTypes(), array_filter($disallowed_types[$parent_layout][$region]['paragraph_types']));
      }

      // Set the updated allowed types.
      $event->setTypes($allowed_types);
    }

    // Check for the numeric limit.
    $numeric_limit = $disallowed_types[$parent_layout][$region]['numeric_limit'] ?? 0;
    if (!empty($numeric_limit)) {
      $paragraph = $parent->getEntity();
      $sectionComponent = $event->getLayout()->getLayoutSection($paragraph);
      $components = $sectionComponent->getComponentsForRegion($region);
      if (count($components) >= $numeric_limit) {
        // Do not allow anything if limit reached.
        $event->setTypes([]);
      }
    }
  }

}
