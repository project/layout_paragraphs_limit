<?php

declare(strict_types = 1);

namespace Drupal\layout_paragraphs_limit\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\ParagraphsType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Layout\LayoutPluginManagerInterface;

/**
 * Implements LayoutParagraphsLimitSettingsForm class.
 *
 * @package Drupal\layout_paragraphs_limit\Form
 */
class LayoutParagraphsLimitSettingsForm extends ConfigFormBase {

  /**
   * The Layouts Manager.
   *
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  protected $layoutPluginManager;

  /**
   * LayoutParagraphsLimitSettingsForm constructor.
   *
   * @param \Drupal\Core\Layout\LayoutPluginManagerInterface $layout_plugin_manager
   *   Core layout plugin manager service.
   */
  public function __construct(LayoutPluginManagerInterface $layout_plugin_manager) {
    $this->layoutPluginManager = $layout_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.core.layout')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'layout_paragraphs_limit_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['layout_paragraphs_limit.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get the available layout types.
    $layout_types = $this->layoutPluginManager->getLayoutOptions();

    // Get the available paragraph types.
    $paragraph_types = ParagraphsType::loadMultiple();
    $available_layouts = [];
    foreach ($paragraph_types as $paragraph_type) {
      $layout_paragraphs_behavior = $paragraph_type->getBehaviorPlugin('layout_paragraphs');

      if (!empty($layout_paragraphs_behavior->getConfiguration())) {
        $layout_paragraphs_configuration = $layout_paragraphs_behavior->getConfiguration();
        $available_layouts = array_merge($available_layouts, $layout_paragraphs_configuration['available_layouts']);
      }
    }

    // Get the disallowed types for each layout from the module's config.
    $config = $this->config('layout_paragraphs_limit.settings');
    $disallowed_types = $config->get('disallowed_types') ?: [];

    // Build the form.
    $form = parent::buildForm($form, $form_state);

    $form['description'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Select the types of paragraphs you would like to enable or disable per Layout region. Select none to allow all paragraph types. Notice that there might be additional filters on paragraph host fields.'),
    ];

    foreach ($layout_types as $group) {
      foreach ($group as $name => $label) {
        if (!array_key_exists($name, $available_layouts)) {
          continue;
        }
        $layout_definition = $this->layoutPluginManager->getDefinition($name);
        $layout_regions = $layout_definition->getRegions();

        $form['disallowed_types'][$name] = [
          '#type' => 'details',
          '#title' => $this->t('Layout: %layout', [
            '%layout' => $label,
          ]),
        ];

        foreach ($layout_regions as $region_key => $region) {
          $form['disallowed_types'][$name][$region_key] = [
            '#type' => 'details',
            '#title' => $this->t('Region: %region', [
              '%region' => $region['label'],
            ]),
            'negate' => [
              '#type' => 'radios',
              '#title' => $this->t('Which Paragraph types should be allowed?'),
              '#options' => [
                1 => $this->t('Include the selected below'),
                0 => $this->t('Exclude the selected below'),
              ],
              '#default_value' => empty($disallowed_types[$name][$region_key]['negate']) ? 0 : 1,
              '#parents' => ['disallowed_types', $name, $region_key, 'negate'],
            ],
            'disallowed_types' => [
              '#type' => 'checkboxes',
              '#multiple' => TRUE,
              '#title' => $this->t('Paragraph types'),
              '#options' => array_map(function ($type) {
                return $type->label();
              }, $paragraph_types),
              '#default_value' => $disallowed_types[$name][$region_key]['paragraph_types'] ?? [],
              '#parents' => ['disallowed_types', $name, $region_key, 'paragraph_types'],
            ],
            'numeric_limit' => [
              '#type' => 'number',
              '#title' => $this->t('Limit total number of components in this region'),
              '#description' => $this->t('Set to 0 to allow unlimited components.'),
              '#min' => 0,
              '#step' => 1,
              '#default_value' => $disallowed_types[$name][$region_key]['numeric_limit'] ?? 0,
              '#parents' => ['disallowed_types', $name, $region_key, 'numeric_limit'],
            ],
          ];
        }
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $disallowed_types = $values['disallowed_types'];
    $disallowed_types = $this->massageFormValues($disallowed_types);

    // Save the disallowed types for each layout to the module's config.
    $config = $this->configFactory->getEditable('layout_paragraphs_limit.settings');
    $config->set('disallowed_types', $disallowed_types);
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Preprocesses form values before saving.
   *
   * "negate" key is cast to boolean and zero is preserved.
   * "numeric_limit" key is cast to integer and zero is preserved.
   * All other keys are regions and filtered out if value == 0.
   *
   * @param array $values
   *   Checkbox values.
   *
   * @return array
   *   Values array.
   */
  private function massageFormValues(array $values) {
    foreach ($values as $key => $value) {
      if ($key == 'negate') {
        $values[$key] = (bool) $value;
      }
      if ($key == 'numeric_limit') {
        $values[$key] = (int) $value;
      }
      if ($value == 0 && !in_array($key, ['negate', 'numeric_limit'])) {
        unset($values[$key]);
      }
      if (is_array($value)) {
        $values[$key] = $this->massageFormValues($value);
      }
    }

    return $values;
  }

}
