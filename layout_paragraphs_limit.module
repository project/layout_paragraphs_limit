<?php

declare(strict_types = 1);

/**
 * @file
 * Contains hooks for the layout paragraphs limit module.
 */

use Drupal\layout_paragraphs\LayoutParagraphsLayout;

/**
 * Implements hook_preprocess_layout_paragraphs_builder_controls().
 */
function layout_paragraphs_limit_preprocess_layout_paragraphs_builder_controls(array &$variables) {
  $config = \Drupal::config('layout_paragraphs_limit.settings');
  $disallowedTypes = $config->get('disallowed_types');

  $variables['#attached']['library'][] = 'layout_paragraphs_limit/move_errors';
  $variables['#attached']['drupalSettings']['layoutParagraphsLimit'] = [
    'disallowedTypes' => $disallowedTypes,
  ];

  $variables['controls']['duplicate_link']['#access'] = layout_paragraphs_limit_duplicate_access($variables['layout_paragraphs_layout'], $variables['uuid']);
}

/**
 * Manage Duplicate control access depending on region limits.
 *
 * @param \Drupal\layout_paragraphs\LayoutParagraphsLayout $layout
 *   The layout.
 * @param $component_uuid
 *   UUID of the component.
 *
 * @return bool
 */
function layout_paragraphs_limit_duplicate_access(LayoutParagraphsLayout $layout, $component_uuid) {
  $component = $layout->getComponentByUuid($component_uuid);

  if ($component->isLayout()) {
    return TRUE;
  }

  $region_configurations = \Drupal::config('layout_paragraphs_limit.settings')->get('disallowed_types');
  $region = $component->getRegion();
  
  $parent_uuid = $component->getParentUuid();
  if (empty($parent_uuid)) {
    return TRUE;
  }

  $parent = $layout->getComponentByUuid($parent_uuid);
  if (empty($parent)) {
    return TRUE;
  }

  $layout_id = $parent->getSettings()['layout'] ?? NULL;

  if ($layout_id && $region) {
    $region_limit = $region_configurations[$layout_id][$region]['numeric_limit'] ?? 0;
    if ($region_limit == 0) {
      return TRUE;
    }

    // Check whether the limit has not been exceeded.
    if (layout_paragraphs_limit_count_components_in_region($layout, $parent_uuid, $region) >= $region_limit) {
      return FALSE;
    }
  }

  return TRUE;
}

/**
 * Helper to count components in specified region.
 *
 * @param \Drupal\layout_paragraphs\LayoutParagraphsLayout $layout
 *   The layout.
 * @param $section_uuid
 *   UUID of the section paragraph.
 * @param $region
 *   Machine name of the region.
 *
 * @return int
 */
function layout_paragraphs_limit_count_components_in_region(LayoutParagraphsLayout $layout, $section_uuid, $region) {
  $count = 0;
  foreach ($layout->getComponents() as $component) {
    if ($component->getParentUuid() === $section_uuid && $component->getRegion() === $region) {
      $count++;
    }
  }
  return $count;
}

/**
 * Move disallowed types on layout to each region in this layout.
 */
function layout_paragraphs_limit_update_91001() {
  $config_factory = \Drupal::configFactory();

  $config = $config_factory->getEditable('layout_paragraphs_limit.settings');
  $disallowed_types = $config->get('disallowed_types');
  $layoutPluginManager = \Drupal::service('plugin.manager.core.layout');
  $layoutDefinitions = $layoutPluginManager->getDefinitions();
  foreach ($disallowed_types as $lid => $layout) {
    // Load possible regions for each layout.
    $layout_regions = $layoutDefinitions[$lid]->getRegions();
    $types = $disallowed_types[$lid];
    // Move disallowed types of layout to each region in this layout.
    $layout = [];
    foreach ($layout_regions as $rid => $region) {
      // First make sure the config doesn't already contain the region keys.
      if (!isset($types[$rid])) {
        $layout[$rid] = $types;
      }
      else {
        $layout[$rid] = $types[$rid];
      }
      $disallowed_types[$lid] = $layout;
    }
  }
  $config->set('disallowed_types', $disallowed_types);
  $config->save();
}

/**
 * Convert all "disallowed" selections into allowlists or blocklists.
 */
function layout_paragraphs_limit_update_91002() {
  $config_factory = \Drupal::configFactory();

  $config = $config_factory->getEditable('layout_paragraphs_limit.settings');
  $disallowed_types = $config->get('disallowed_types');
  $layoutPluginManager = \Drupal::service('plugin.manager.core.layout');
  $layoutDefinitions = $layoutPluginManager->getDefinitions();
  foreach ($disallowed_types as $lid => $layout) {
    $layout_regions = $layoutDefinitions[$lid]->getRegions();

    foreach ($layout_regions as $rid => $region) {
      if (
        isset($disallowed_types[$lid][$rid]['paragraph_types']) &&
        isset($disallowed_types[$lid][$rid]['negate']) &&
        is_array($disallowed_types[$lid][$rid]['paragraph_types']) &&
        is_scalar($disallowed_types[$lid][$rid]['negate'])
      ) {
        // This region has a scalar "negate" and an array "paragraph types",
        // which means those are new config values and not region names.
        $negate = empty($disallowed_types[$lid][$rid]['negate']) ? FALSE : TRUE;
        $oldTypes = $disallowed_types[$lid][$rid]['paragraph_types'];
      }
      else {
        // Else update to structure with default "negate = FALSE".
        $negate = FALSE;
        $oldTypes = $disallowed_types[$lid][$rid] ?? [];
      }
      $disallowed_types[$lid][$rid] = [
        'negate' => $negate,
        'paragraph_types' => $oldTypes,
      ];
    }
  }
  $config->set('disallowed_types', $disallowed_types);
  $config->save();
}

/**
 * Add numeric limit per region.
 */
function layout_paragraphs_limit_update_91003() {
  $config_factory = \Drupal::configFactory();

  $config = $config_factory->getEditable('layout_paragraphs_limit.settings');
  $disallowed_types = $config->get('disallowed_types');
  $layoutPluginManager = \Drupal::service('plugin.manager.core.layout');
  $layoutDefinitions = $layoutPluginManager->getDefinitions();
  foreach ($disallowed_types as $lid => $layout) {
    $layout_regions = $layoutDefinitions[$lid]->getRegions();
    foreach ($layout_regions as $rid => $region) {
      $disallowed_types[$lid][$rid]['numeric_limit'] = $disallowed_types[$lid][$rid]['numeric_limit'] ?? 0;
    }
  }
  $config->set('disallowed_types', $disallowed_types);
  $config->save();
}
